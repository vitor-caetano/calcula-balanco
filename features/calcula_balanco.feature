Feature: Calcula Balanço
  Calcula o balanço da conta corrente de um conjunto de clientes

  Scenario: Basic UX
    When I get help for "calcula_balanco"
    Then the exit status should be 0
    And the banner should be present
    And there should be a one line summary of what the app does
    And the banner should include the version
    And the banner should document that this app takes options
    And the following options should be documented:
      | --version |
    And the banner should document that this app's arguments are:
      | arquivo_contas     | which is required |
      | arquivo_transacoes | which is required |

  Scenario: Testing with valid input files
    Given the file "contas.csv" does exist
    Given the file "transacoes.csv" does exist
    When I successfully run `calcula_balanco ../../contas.csv ../../transacoes.csv`
    Then the stdout should contain:
  """
  44,-9611
  35,57485
  76,-47941
  103,109276
  155,6920
  201,10000
  """
