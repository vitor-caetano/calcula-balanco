Given /^the file "([^"]*)" does exist$/ do |file|
 File.exists?(File.join(File.dirname(__FILE__),'..','..', file)).should == true
end
