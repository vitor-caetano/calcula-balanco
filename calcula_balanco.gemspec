# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'calcula_balanco/version'

Gem::Specification.new do |spec|
  spec.name          = "calcula_balanco"
  spec.version       = CalculaBalanco::VERSION
  spec.authors       = ["vitor"]
  spec.email         = ["vitor.e.caetano@gmail.com"]
  spec.summary       = "Calcula o balanço da conta corrente de um conjunto de clientes."
  spec.description   = "Write a longer description. Optional."
  spec.homepage      = ""
  spec.license       = "apache"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency('rdoc')
  spec.add_development_dependency('cucumber')
  spec.add_development_dependency('aruba')
  spec.add_development_dependency('rake')
  spec.add_development_dependency('test-unit')
  spec.add_development_dependency('mocha')
  spec.add_dependency('methadone', '~> 1.9.0')
end
