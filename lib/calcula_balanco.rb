require "calcula_balanco/version"
require 'csv'

module CalculaBalanco
  class ProcessaArquivos
    def exec(arquivo_contas, arquivo_transacaoes)
      @arquivo_contas = arquivo_contas
      @arquivo_transacoes = arquivo_transacaoes

      validate_input_files
      load_contas
      process_transacoes
      output_contas

      @contas
    end

    private

    def load_contas
      @contas = CSV.read(@arquivo_contas).inject({}) do |hash, value|
        id = value.first.to_i
        if id <= 0
          warn "Id da conta inválido #{value.first} em entrada do arquivo #{@arquivo_contas}"
        else
          hash[id] = value.last.to_i
        end
        hash
      end
    end

    def output_contas
      @contas.each do |key, value|
        puts "#{key},#{value}"
      end
    end

    def process_transacoes
      CSV.foreach(@arquivo_transacoes) do |row|
        id = row.first.to_i
        if id <=0
          warn "Id da conta inválido #{row.first} em entrada do arquivo #{@arquivo_transacoes}"
          next
        end
        valor = row.last.to_i
        if @contas[id].nil?
          warn "Id da conta não encontrado #{row.first} em entrada do arquivo #{@arquivo_transacoes}"
          next
        end
        @contas[id] += valor
        @contas[id] -= 500 if @contas[id] < 0 && valor < 0
      end
    end

    def validate_input_files
      message_contas = File.exists?(@arquivo_contas) ? "" : "Arquivo de contas não encontrado - #{@arquivo_contas}"
      message_transacoes = File.exists?(@arquivo_transacoes) ? "" : "Arquivo de transações não encontrado - #{@arquivo_transacoes}"

      return if message_contas.empty? && message_transacoes.empty?

      message = ""
      message = message_contas unless message_contas.empty?
      if !message_contas.empty? && !message_transacoes.empty?
        message += "\n"
      end
      message += message_transacoes

      raise message
    end
  end
end


