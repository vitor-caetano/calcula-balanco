# Calcula Balanço

Calcula o balanço da conta corrente de um conjunto de clientes através da linha de commando.

## Installation

Git clone from application's repository:

    git clone https://bitbucket.org/vitor-caetano/calcula-balanco.git

And then move to repo directory and install dependencies with bundle:

    $ cd calcula-balanco/
    $ bundle install

Install calcula_balanco package:

    $ rake install

## Usage

Get usage instructions with help:

    $ calcula_balanco --help
    
Run with available seed files:

    $ calcula_balanco contas.csv transacoes.csv
    
## Testing

Run unit tests with:

    $ rake test

Run acceptance tests with:

    $ rake features
    
Run both tests:

    $ rake

## Dependencies

* This is a command line application built with [Methadone](http://github.com/davetron5000/methadone)
* Test-driven with [Cucumber](https://github.com/cucumber/cucumber) Steps, built on [Aruba](https://github.com/cucumber/aruba)
* Also unit tested with [test-unit](https://github.com/test-unit/test-unit) and [Mocha](https://github.com/freerange/mocha)