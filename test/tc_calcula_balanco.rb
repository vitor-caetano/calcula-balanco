require 'test/unit'
require 'mocha/test_unit'

require_relative '../test_unit_extensions'
require_relative '../lib/calcula_balanco'

class CalculaBalancoTest < Test::Unit::TestCase
  must "raise error when input files not found" do
    File.stubs(:exists?).returns(false)
    ex = assert_raises RuntimeError do
      CalculaBalanco::ProcessaArquivos.new.exec("contas.csv", "transacoes.csv")
    end
    expected = "Arquivo de contas não encontrado - contas.csv\nArquivo de transações não encontrado - transacoes.csv"
    assert_equal expected, ex.message
  end

  must "process when input files found" do
    File.stubs(:exists?).returns(true)
    CSV.stubs(:read).with("contas.csv").returns(
        [[44, 2234],
         [35, 56985],
         [76, 8745],
         [103, 54638],
         [155, 3460],
         [201, 10000]])
    CSV.stubs(:read).with("transacoes.csv").returns(
        [[44, 1000],
         [35, 500],
         [76, 1000],
         [76, -58589],
         [76, -100],
         [44, -12345],
         [103, 54638],
         [155, 3460],
         [222, 304],
         [76, 2003]])

    expected = CalculaBalanco::ProcessaArquivos.new.exec("contas.csv", "transacoes.csv")
    assert_equal expected, {44 => -9611,
                            35 => 57485,
                            76 => -47941,
                            103 => 109276,
                            155 => 6920,
                            201 => 10000}
  end

  def teardown
    File.unstub(:exists?)
    CSV.unstub(:read)
  end
end
